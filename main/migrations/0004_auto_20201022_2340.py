# Generated by Django 3.1.2 on 2020-10-22 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201022_2326'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='tanggal',
            field=models.CharField(default='Anytime', max_length=50),
        ),
        migrations.AlterField(
            model_name='event',
            name='lokasi',
            field=models.CharField(default='Anywhere', max_length=50),
        ),
        migrations.AlterField(
            model_name='event',
            name='waktu',
            field=models.CharField(default='Anytime', max_length=50),
        ),
    ]
