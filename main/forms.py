from django import forms
from .models import Event, Attendee
from crispy_forms.helper import FormHelper
from datetime import datetime

class EventForm(forms.ModelForm):
    
    def clean_tanggal(self):
        data = self.cleaned_data["tanggal"]
        if data == "Anytime": return data
        try:
            data = datetime.strptime(data, '%H:%M %d-%m-%Y')
        except:
            raise forms.ValidationError(
                u'Tanggal/waktu anda tidak valid!! "%s"' % data)
        
        if datetime.now() >= data:
            raise forms.ValidationError(u'Tanggal/waktu anda tidak valid!! "%s"' % data)
        return data
        
    class Meta:
        model = Event
        fields = "__all__"
        widgets = {
            'tanggal' : forms.DateTimeInput(),
            'peserta': forms.HiddenInput()
        }

class AttendeeForm(forms.ModelForm):
    class Meta:
        model = Attendee
        fields = '__all__'
