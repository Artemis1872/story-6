from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from .models import Event, Attendee
from .forms import EventForm, AttendeeForm
from datetime import datetime
# from selenium import webdriver


# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)
        
        response = self.client.get(reverse('main:event-add'))
        self.assertEqual(response.status_code, 200)
        
        peserta = Attendee.objects.create(nama="test_peserta")
        event_test = Event.objects.create(
            nama="test_event",
            deskripsi="test_description",
            lokasi="Anywhere",
            nama_penyelenggara="asdf",
            tanggal="Anytime",
        )
        
        response = self.client.get(reverse('main:event-detail', args=(event_test.id,)))
        self.assertEqual(response.status_code, 200)
        
        response = self.client.get(reverse('main:event-join', args=(event_test.id,)))
        self.assertEqual(response.status_code, 200)
        
    def test_event_forms(self):
        form_data = {
            "nama" : "test_form",
            "deskripsi" : "this is a dummy data",
            "lokasi" : "Anywhere",
            "nama_penyelenggara" : "Django UnitTest",
            "tanggal" : "23:00 23-10-2030",
            "peserta" : ""
        }
        form = EventForm(data=form_data)
        self.assertTrue(form.is_valid())
        
        response = self.client.post("/new-event", data=form_data)
        self.assertEqual(response.status_code, 302)
        
        form_data["tanggal"] = "Anytime"
        form = EventForm(data=form_data)
        self.assertTrue(form.is_valid())
        
        form_data["tanggal"] = "14:29 23-10-1020"
        form = EventForm(data=form_data)
        self.assertFalse(form.is_valid())
        
        response = self.client.post("/new-event", data=form_data)
        self.assertContains(response, "tidak valid!")
        
        form_data["tanggal"] = "23-10-3020"
        self.assertFalse(form.is_valid())
        
        form_data["tanggal"] = "asdf"
        self.assertFalse(form.is_valid())
        
    def test_attendee_form(self):
        event_test = Event.objects.create(
            nama="test_event",
            deskripsi="test_description",
            lokasi="Anywhere",
            nama_penyelenggara="asdf",
            tanggal="Anytime",
        )
        
        data = {
            "nama" : "test_attendee"
        }
        
        response = self.client.post(
            reverse('main:event-join', args=(event_test.id,)), 
            data=data
        )
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get(
            reverse('main:event-detail', args=(event_test.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, data["nama"])
        
        data["nama"] = "   test_attendee   "
        response = self.client.post(
            reverse('main:event-join', args=(event_test.id,)),
            data=data
        )
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get(
            reverse('main:event-detail', args=(event_test.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, data["nama"].strip())
        
# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
