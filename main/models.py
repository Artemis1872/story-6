from django.db import models
from datetime import datetime

# Create your models here.
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.db import models

# Create your models here.
class Attendee(models.Model):
    
    name = "Attendee"
    
    nama = models.CharField(max_length=50, unique=True)
    
    class Meta:
        verbose_name = "Attendee"
        verbose_name_plural = "Attendees"

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse("Attendee_detail", kwargs={"pk": self.pk})


class Event(models.Model):
    
    name = "Event"

    nama = models.CharField(max_length=50)
    deskripsi = models.TextField(max_length=500)
    lokasi = models.CharField(max_length=50, default="Anywhere")
    nama_penyelenggara = models.CharField(max_length=100)
    tanggal = models.CharField(
        max_length=50, default=datetime.now().strftime("%H:%M %d-%m-%Y"))
    peserta = models.ManyToManyField(Attendee, blank=True)

    waktu_dibuat= models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Event_detail", kwargs={"pk": self.pk})
