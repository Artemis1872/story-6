from django.shortcuts import render, redirect
from .forms import EventForm, AttendeeForm
from .models import Event, Attendee

def home(request):
    context = {
        'events' : Event.objects.all()
    }
    return render(request, 'main/home.html', context=context)

def event_detail(request, id):
    """
    Event Detail
    """
    context = {
        'event' : Event.objects.get(id=id)
    }
    return render(request, 'main/detail-event.html', context=context)


def event_join(request, id):
    if request.method == "POST":
        event = Event.objects.get(id=id)
        nama = request.POST["nama"].strip()
        new, status = Attendee.objects.get_or_create(nama=nama)
        if status:
            new.save()
            event.peserta.add(new)
        else:
            try:
                event.peserta.get(nama=nama)
            except:
                event.peserta.add(new)
        return redirect('main:home')
    
    else:
        context = {
            'form' : AttendeeForm(),
        }
        return render(request, 'main/join-event.html', context=context)

def event_add(request):
    context = {
        'form': EventForm(),
    }
    
    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('main:home')
        context['form'] = form
        return render(request, 'main/new-event.html', context=context)
    else:
        return render(request, 'main/new-event.html', context=context)
