from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('<int:id>', views.event_detail, name='event-detail'),
   # path('delete-event/<int:id>', views.event_delete, name='event-delete'),
    path('new-event', views.event_add, name='event-add'),
    path('join-event/<int:id>', views.event_join, name='event-join'),
]
